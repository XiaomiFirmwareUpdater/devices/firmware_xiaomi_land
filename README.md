## Xiaomi Firmware Packages For Redmi 3S/Prime/3X (land)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 303 | HM3S | Xiaomi Redmi 3S/Prime/3X | land |
| 303 | HM3SGlobal | Xiaomi Redmi 3S/Prime/3X Global | land |

### XDA Support Thread For land:
[Go here](https://forum.xda-developers.com/xiaomi-redmi-3s/development/firmware-xiaomi-redmi-3s-t3760880)

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
